#!/usr/bin/env python3
import fitz
#import os
def pdfmergedoc(filelist,outfilename):
    docoutput=fitz.open()
    for name in filelist:
        try:
            doc=fitz.open(name)
            docoutput.insert_pdf(doc)
            doc.close()
        except:
            print('merge {} fail'.format(name))
    docoutput.save(outfilename)
    docoutput.close()

    #print('merge is over')
def pdfmergepage(namelist,outfilename):
    docoutput = fitz.open()
    for name in namelist:
        try:
            doc=fitz.open(name[0])
            docoutput.insert_pdf(doc,from_page=name[1],to_page=name[2])
            doc.close()
        except:
            print('merge/spplit {} fail'.format(name[0]))
    docoutput.save(outfilename)
    docoutput.close()
    #print('merge is over')




if __name__=='__main__':
    filelist=[['out.pdf',0,1],['1.pdf',0,0]]
    outfilename='pageout.pdf'
    #pdfmergedoc(filelist,outfilename)
    pdfmergepage(filelist, outfilename)
