# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'g:\Python\2021_5_pdfmaster\pdfmaster\split.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(20)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.sb_starpage = QtWidgets.QSpinBox(self.groupBox)
        self.sb_starpage.setObjectName("sb_starpage")
        self.gridLayout.addWidget(self.sb_starpage, 0, 1, 1, 1)
        self.cb_firstpage = QtWidgets.QCheckBox(self.groupBox)
        self.cb_firstpage.setObjectName("cb_firstpage")
        self.gridLayout.addWidget(self.cb_firstpage, 0, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.sb_endpage = QtWidgets.QSpinBox(self.groupBox)
        self.sb_endpage.setObjectName("sb_endpage")
        self.gridLayout.addWidget(self.sb_endpage, 1, 1, 1, 1)
        self.cb_endpage = QtWidgets.QCheckBox(self.groupBox)
        self.cb_endpage.setObjectName("cb_endpage")
        self.gridLayout.addWidget(self.cb_endpage, 1, 2, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.verticalLayout.setStretch(1, 1)
        self.label_2.setBuddy(self.sb_starpage)
        self.label_3.setBuddy(self.sb_endpage)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "分割参数"))
        self.label_2.setText(_translate("Dialog", "起始页"))
        self.cb_firstpage.setText(_translate("Dialog", "从第一页"))
        self.label_3.setText(_translate("Dialog", "终止页"))
        self.cb_endpage.setText(_translate("Dialog", "到最后"))
