#!/usr/bin/env python3
import sys
import fitz
from PyQt5.QtWidgets import QApplication,QMainWindow,QListWidgetItem,QFileDialog,QDialog,QMenu,QMessageBox
from PyQt5.QtCore import pyqtSlot,Qt,QDir
from PyQt5.QtGui import QIcon,QPixmap,QCursor,QImage

from Ui_mainwindow import Ui_MainWindow
from pdfmerge import pdfmergedoc, pdfmergepage
from splitsetting import QUiSplit

class Qmywindow(QMainWindow):
    def __init__(self,parent=None):
        super().__init__(parent)
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        # self.splitfile=''
        # self.splitstartpage=0
        # self.splitendpage=0
        self.curpage=QPixmap()
        self.pixrate=1
        self.flag=(Qt.ItemIsSelectable | Qt.ItemIsUserCheckable|Qt.ItemIsEnabled)
        self.__setactionsforbutton()

        self.ui.le_currentpage.setText('')
        self.ui.pdfpix.setText('没有预览')
        self.ui.LB_totalpage.setText('')
        self.ui.listWidget.clear()
        self.ui.listWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.act_split.setEnabled(False)




    #链接action
    def __setactionsforbutton(self):
        # self.ui.tb_actzoom.setDefaultAction(self.ui.act_zoom)
        # self.ui.tb_actzoomout.setDefaultAction(self.ui.act_zoomout)
        # self.ui.tb_actreal.setDefaultAction(self.ui.act_real)
        # self.ui.tb_actrfiw.setDefaultAction(self.ui.act_fitwindow)
        self.ui.tb_lastpage.setDefaultAction(self.ui.act_lastpage)
        self.ui.tb_nextpage.setDefaultAction(self.ui.act_nextpage)

    #增加右键
    def on_listWidget_customContextMenuRequested(self,pos):
        menuList = QMenu(self)
        menuList.addAction(self.ui.act_open)
        menuList.addAction(self.ui.act_selall)
        menuList.addAction(self.ui.act_inv)
        menuList.addAction(self.ui.act_cansel)
        menuList.exec(QCursor.pos())
    #添加文件夹
    @pyqtSlot()
    def on_act_addfolder_triggered(self):
        icon = QIcon(':/images/1.jpg')
        curdir = QDir.currentPath()
        adir=QFileDialog.getExistingDirectory(self,'选择要添加的文件夹',curdir, QFileDialog.ShowDirsOnly)
        dirobj=QDir(adir)
        filelist=dirobj.entryList(QDir.Files|QDir.NoDotAndDotDot)
        for file in filelist:
            if file.lower().endswith('.pdf'):
                aitem = QListWidgetItem()
                filename=adir+r'/'+file
                # filename=QDir(file)
                # filename=filenme.absolutePath()

                aitem.setText(filename)
                aitem.setIcon(icon)
                aitem.setCheckState(Qt.Checked)
                aitem.setFlags(self.flag)
                self.ui.listWidget.addItem(aitem)


    #添加文件
    @pyqtSlot()
    def on_act_add_triggered(self):
        icon=QIcon(':/images/1.jpg')
        curdir=QDir.currentPath()
        filelist,flt=QFileDialog.getOpenFileNames(self,'选择pdf文件',curdir,'PDF Files (*.pdf)')
        for file in filelist:
            aitem=QListWidgetItem()
            aitem.setText(file)
            aitem.setIcon(icon)
            aitem.setCheckState(Qt.Checked)
            aitem.setFlags(self.flag)
            self.ui.listWidget.addItem(aitem)

            # print(file)
    #合并文件
    @pyqtSlot()
    def on_act_merage_triggered(self):
        if self.ui.listWidget.count()==0:
            QMessageBox.warning(self,'Warning','请先选择文件')
            return
        curpath=QDir.currentPath()
        title='输入合并的文件名称'
        filt='PDF Files (*.pdf)'
        outfilename,flt=QFileDialog.getSaveFileName(self,title,curpath,filt)
        if outfilename=='':
            return
        filelist=[]
        for i in range(self.ui.listWidget.count()):
            file=self.ui.listWidget.item(i).text()
            filelist.append(file)
            # print(file)
        try:
            pdfmergedoc(filelist,outfilename)
        # print('OK')
        except:
            QMessageBox.warning('Warning','合并不能完成')

    #全选
    @pyqtSlot()
    def on_act_selall_triggered(self):
        for i in range(self.ui.listWidget.count()):
            aitem=self.ui.listWidget.item(i)
            aitem.setCheckState(Qt.Checked)

    #不选
    @pyqtSlot()
    def on_act_cansel_triggered(self):
        for i in range(self.ui.listWidget.count()):
            aitem=self.ui.listWidget.item(i)
            aitem.setCheckState(Qt.Unchecked)

    #反选
    @pyqtSlot()
    def on_act_inv_triggered(self):
        try:
            for i in range(self.ui.listWidget.count()):
                aitem = self.ui.listWidget.item(i)
                if (aitem.checkState() != Qt.Checked):
                    aitem.setCheckState(Qt.Checked)
                else:
                    aitem.setCheckState(Qt.Unchecked)

        except:
            QMessageBox.critical(self,'Error','请先选择需要分割的文件')


    #删除
    @pyqtSlot()
    def on_act_del_triggered(self):
        row=self.ui.listWidget.currentRow()
        self.ui.listWidget.takeItem(row)

    @pyqtSlot()
    def on_act_open_triggered(self):
        row=self.ui.listWidget.currentRow()
        self.opfile=self.ui.listWidget.item(row).text()
        self.opendoc=fitz.open(self.opfile)
        self.ui.statusbar.showMessage('正在打开：'+self.opfile)
        self.ui.act_split.setEnabled(True)
        #此处打开Fitz对象，存在open doc中
        self.pdfcount=self.opendoc.page_count
        self.ui.LB_totalpage.setText(r'/'+str(self.pdfcount))
        curpageno = 0
        page=self.opendoc[curpageno]
        mat=fitz.Matrix(self.pixrate,self.pixrate)
        self.curpage=page.get_pixmap(matrix=mat)
        self.ui.le_currentpage.setText(str(curpageno+1))
        fmt = QImage.Format_RGBA8888 if self.curpage.alpha else QImage.Format_RGB888
        qtimg = QImage(self.curpage.samples, self.curpage.width,self.curpage.height, self.curpage.stride, fmt)
        self.labimag=QPixmap.fromImage(qtimg)
        self.ui.pdfpix.setPixmap(self.labimag)


    #选中变色
    def on_listWidget_currentItemChanged(self,current,previous):
        if current != None:
            current.setBackground(Qt.blue)
        if previous !=None:
            previous.setBackground(Qt.white)
    #翻页特性
    @pyqtSlot()
    def on_act_lastpage_triggered(self):
        prepage=int(self.ui.le_currentpage.text())
        if prepage==1:
            return
        self.ui.le_currentpage.setText(str(prepage-1))
        curpage=prepage-2
        page=self.opendoc[curpage]
        mat = fitz.Matrix(self.pixrate, self.pixrate)
        self.curpage = page.get_pixmap(matrix=mat)
        fmt = QImage.Format_RGBA8888 if self.curpage.alpha else QImage.Format_RGB888
        qtimg = QImage(self.curpage.samples, self.curpage.width, self.curpage.height, self.curpage.stride, fmt)
        self.labimag = QPixmap.fromImage(qtimg)
        self.ui.pdfpix.setPixmap(self.labimag)

    @pyqtSlot()
    def on_act_nextpage_triggered(self):
        prepage = int(self.ui.le_currentpage.text())
        if prepage == self.pdfcount:
            return
        self.ui.le_currentpage.setText(str(prepage +1))
        curpage = prepage
        page = self.opendoc[curpage]
        mat = fitz.Matrix(self.pixrate, self.pixrate)
        self.curpage = page.get_pixmap(matrix=mat)
        fmt = QImage.Format_RGBA8888 if self.curpage.alpha else QImage.Format_RGB888
        qtimg = QImage(self.curpage.samples, self.curpage.width, self.curpage.height, self.curpage.stride, fmt)
        self.labimag = QPixmap.fromImage(qtimg)
        self.ui.pdfpix.setPixmap(self.labimag)

    #放大
    # @pyqtSlot()
    # def on_act_zoom_triggered(self):
    #     self.pixrate=self.pixrate*1.2
    #     W=self.pixrate*self.labimag.width()
    #     H=self.pixrate*self.labimag.height()
    #     self.labimag=self.labimag.scaled(W,H)
    #     self.ui.pdfpix.setPixmap(self.labimag)
    @pyqtSlot()
    def on_act_up_triggered(self):
        if self.ui.listWidget.count() == 0:
            return
        curow=self.ui.listWidget.currentRow()
        if curow==0:
            return
        prerow=curow-1
        atext=self.ui.listWidget.item(curow).text()
        self.ui.listWidget.item(curow).setText(self.ui.listWidget.item(prerow).text())
        self.ui.listWidget.item(prerow).setText(atext)
        self.ui.listWidget.setCurrentRow(prerow)

    @pyqtSlot()
    def on_act_down_triggered(self):
        if self.ui.listWidget.count()==0:
            return
        curow=self.ui.listWidget.currentRow()
        if curow==self.ui.listWidget.count()-1:
            return
        nextrow=curow+1
        atext=self.ui.listWidget.item(curow).text()
        self.ui.listWidget.item(curow).setText(self.ui.listWidget.item(nextrow).text())
        self.ui.listWidget.item(nextrow).setText(atext)
        self.ui.listWidget.setCurrentRow(nextrow)

    #分割的部分
    @pyqtSlot()
    def on_act_split_triggered(self):
        # if self.opendoc==None:
        #     QMessageBox.warning('Warning','请先选择一个文件')
        sptab=QUiSplit(self.pdfcount)
        ret=sptab.exec()
        if ret==QDialog.Accepted:
            sptab.outputdata()
            # print(str(sptab.outpar[0])+'/'+str(sptab.outpar[1]))
            startpage=sptab.outpar[0]
            endpage=sptab.outpar[1]
            filename=self.opfile
            name=[]
            namelist=[]
            name.append(filename)
            name.append(startpage)
            name.append(endpage)
            namelist.append(name)
            curpath = QDir.currentPath()
            title = '输入分割的文件名称'
            filt = 'PDF Files (*.pdf)'
            outfilename, flt = QFileDialog.getSaveFileName(self, title, curpath, filt)
            if outfilename == '':
                return
            try:
                pdfmergepage(namelist,outfilename)
            except:
                QMessageBox.warning('Warning','分割失败！')










if __name__=='__main__':
    app=QApplication(sys.argv)
    form=Qmywindow()
    form.show()
    sys.exit(app.exec_())

