import sys
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtCore import Qt
from Ui_split import Ui_Dialog

class QUiSplit(QDialog):
    def __init__(self, totpage,parent=None):
        super().__init__(parent)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowFlags(Qt.MSWindowsFixedSizeDialogHint)
        self.outpar=[]
        self.ui.cb_endpage.setCheckState(Qt.Unchecked)
        self.ui.cb_firstpage.setCheckState(Qt.Unchecked)
        self.totpage=totpage
        self.ui.sb_endpage.setValue(int(self.totpage))
        self.ui.sb_starpage.setValue(1)
        self.ui.cb_firstpage.stateChanged.connect(self.set1stpage)
        self.ui.cb_endpage.stateChanged.connect(self.setendpage)
        self.ui.sb_starpage.valueChanged.connect(self.starpage)
        self.ui.sb_endpage.valueChanged.connect(self.endpage)

    def set1stpage(self):
        self.ui.sb_starpage.setValue(1)

    def setendpage(self):
        self.ui.sb_endpage.setValue(int(self.totpage))

    def starpage(self):
        self.ui.cb_firstpage.setCheckState(Qt.Unchecked)

    def endpage(self):
        self.ui.cb_endpage.setCheckState(Qt.Unchecked)

    def outputdata(self):
        spageno=self.ui.sb_starpage.value()
        endpageno=self.ui.sb_endpage.value()
        self.outpar.append(spageno)
        self.outpar.append(endpageno)








if __name__=='__main__':
    app=QApplication(sys.argv)
    form=QUiSplit(5)
    form.show()
    sys.exit(app.exec_())


